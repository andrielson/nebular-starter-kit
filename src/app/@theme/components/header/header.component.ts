import { Component, OnDestroy, OnInit } from '@angular/core';
import { NbMediaBreakpointsService, NbMenuService, NbSidebarService, NbThemeService } from '@nebular/theme';

import { map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { LayoutService } from '../../services/layout.service';

@Component({
    selector: 'app-header',
    styleUrls: ['./header.component.scss'],
    templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {

    private destroy$: Subject<void> = new Subject<void>();
    userPictureOnly: boolean = false;
    user: string;

    themes = [
        {
            value: 'default',
            name: 'Padrão',
        },
        {
            value: 'dark',
            name: 'Escuro',
        },
        {
            value: 'cosmic',
            name: 'Cósmico',
        },
        {
            value: 'corporate',
            name: 'Corporativo',
        },
    ];

    currentTheme = 'default';

    constructor(private sidebarService: NbSidebarService,
        private menuService: NbMenuService,
        private themeService: NbThemeService,
        //private userService: UsuarioService,
        private layoutService: LayoutService,
        private breakpointService: NbMediaBreakpointsService) {
    }

    ngOnInit() {
        this.currentTheme = this.themeService.currentTheme;

        /* this.userService.getNomeUsuario()
            .pipe(takeUntil(this.destroy$))
            .subscribe((nome: string) => this.user = nome); */
        this.user = 'Usuário Teste';

        const { xl } = this.breakpointService.getBreakpointsMap();
        this.themeService.onMediaQueryChange()
            .pipe(
                map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
                takeUntil(this.destroy$),
            )
            .subscribe((isLessThanXl: boolean) => this.userPictureOnly = isLessThanXl);

        this.themeService.onThemeChange()
            .pipe(
                map(({ name }) => name),
                takeUntil(this.destroy$),
            )
            .subscribe(themeName => this.currentTheme = themeName);
    }

    ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    changeTheme(themeName: string) {
        this.themeService.changeTheme(themeName);
    }

    toggleSidebar(): boolean {
        this.sidebarService.toggle(true, 'menu-sidebar');
        this.layoutService.changeLayoutSize();

        return false;
    }

    navigateHome() {
        this.menuService.navigateHome();
        return false;
    }
}
