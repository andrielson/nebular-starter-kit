import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Início',
    icon: 'home-outline',
    link: '/pages/home',
    home: true,
  },
  {
    title: 'OPÇÕES',
    group: true,
  },
];
