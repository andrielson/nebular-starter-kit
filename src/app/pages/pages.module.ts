import { NgModule } from '@angular/core';

import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { HomeModule } from './home/home.module';

@NgModule({
    imports: [
      PagesRoutingModule,
      ThemeModule,
      HomeModule,
    ],
    declarations: [
      PagesComponent,
    ],
    providers: []
  })
  export class PagesModule { }
