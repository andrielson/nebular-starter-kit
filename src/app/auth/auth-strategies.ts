import { NbPasswordAuthStrategy } from '@nebular/auth';

// Para ver mais opções de definição da estratégia, acesse:
// - https://akveo.github.io/nebular/docs/auth/configuring-a-strategy#setup-api-configuration
// - https://github.com/akveo/nebular/blob/master/src/framework/auth/strategies/password/password-strategy-options.ts

export const AuthStrategy = {
    strategies: [
        NbPasswordAuthStrategy.setup({
            name: 'email',
        }),
    ],
    forms: {},
}